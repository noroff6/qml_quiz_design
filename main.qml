import QtQuick
import QtQuick.Controls

Window {
    width: 640
    height: 480
    visible: true
    color: "pink"
    title: qsTr("Welcome to the quiz game!")

    Component
    {
        id: spacers
        Rectangle{
            width: 25
            height: 50
            color: "pink"
        }
    }

    // Side space
    Loader { sourceComponent: spacers; id: sideRect }

    // Player
    Rectangle{
        id: playerRect
        width: 80
        height: 50
        color: "pink"
        border.color: "grey"
        anchors.left: sideRect.right

        Text {
            id: player
            text: qsTr("Player")
            font.family: "Courier New"
            font.pointSize: 14
        }

        Text {
            id: nameOfPlayer
            text: qsTr("Helena")
            font.family: "Courier New"
            font.pointSize: 14
            anchors.top: player.bottom
        }

    }

    // Points
    Rectangle{
        id: pointsRect
        width: 80
        height: 50
        color: "pink"
        border.color: "grey"
        anchors.left: playerRect.right

        Text {
            id: points
            text: qsTr("Points")
            font.family: "Courier New"
            font.pointSize: 14
        }
        Text {
            id: numberPoints
            text: qsTr("0")
            font.family: "Courier New"
            font.pointSize: 14
            anchors.top: points.bottom

        }
    }

    // Space rectangle
    Rectangle{
        id: spaceRect
        width: 330
        height: 50
        color: "pink"
        anchors.left: pointsRect.right
    }

    // Time left
    Rectangle{
        id: timeRect
        width: 100
        height: 50
        color: "pink"
        border.color: "grey"
        anchors.left: spaceRect.right

        Text {
            id: timeText
            text: qsTr("Time left")
            font.family: "Courier New"
            font.pointSize: 14
        }
        Text {
            id: digits
            text: qsTr("6")
            font.family: "Courier New"
            font.pointSize: 14
            anchors.top: timeText.bottom
        }
    }

    // Right space rect
    Loader { sourceComponent: spacers; id: sideRightRect; anchors.left: timeRect.right }

    // Space rectangle
    Rectangle{
        id: spaceRectFull
        width: 640
        height: 50
        color: "pink"
        anchors.top: timeRect.bottom
    }

    // Question rectangle
    Rectangle{
        id: questionRect
        width: 600
        height: 40
        color: "white"
        anchors.top: spaceRectFull.bottom
        //anchors.topMargin: 1
        anchors.horizontalCenter: spaceRectFull.horizontalCenter

        Text {
            id: questionText
            text: qsTr("Who created C++?")
            font.family: "Courier New"
            font.pointSize: 14
            font.bold: true
            padding: 10
        }
    }

    // Space rectangle middle
    Rectangle{
        id: spaceRectMiddle
        width: 640
        height: 50
        color: "pink"
        anchors.top: questionRect.bottom
    }

    // Space for Button/Answers
    Loader { sourceComponent: spacers; id: spaceButAnsw; anchors.top: spaceRectMiddle.bottom }

    // Button A
    Rectangle{
        id: buttonRect
        width: 100
        height: 40
        color: "yellow"
        anchors.top: spaceRectMiddle.bottom
        anchors.left: spaceButAnsw.right

        Text {
            id: buttonText
            anchors.fill: parent
            text: qsTr("A")
            font.family: "Courier New"
            font.pointSize: 14
            font.bold: true
            padding: 10
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    // Component for answers
    Component
    {
        id: answersBox
        Rectangle{
            id: rectAnsBox
            width: 150
            height: 35
            color: "white"
        }

    }

    // Loader for answers
     Loader {
         id: firstAnswerBox
         sourceComponent: answersBox
         anchors.top: spaceRectMiddle.bottom
         anchors.left: buttonRect.right
         anchors.leftMargin: 20
     }

     // Text for first answer A
     Text {
         id: buttonText2
         text: qsTr("Donald Duck")
         font.family: "Courier New"
         font.pointSize: 10
         padding: 10
         color: "black"
         anchors.top: spaceRectMiddle.bottom
         anchors.left: buttonRect.right
         anchors.leftMargin: 20
     }



     // Button B
     Rectangle{
         id: buttonRectB
         width: 100
         height: 40
         color: "yellow"
         anchors.top: spaceRectMiddle.bottom
         anchors.left: firstAnswerBox.right
         anchors.leftMargin: 20

         Text {
             id: buttonTextB
             anchors.fill: parent
             text: qsTr("B")
             font.family: "Courier New"
             font.pointSize: 14
             font.bold: true
             padding: 10
             horizontalAlignment: Text.AlignHCenter
             verticalAlignment: Text.AlignVCenter
         }
     }

     // Loader for answers
      Loader {
          sourceComponent: answersBox
          anchors.top: spaceRectMiddle.bottom
          anchors.left: buttonRectB.right
          anchors.leftMargin: 20
      }

      // Text for second answer
      Text {
          id: buttonText3
          text: qsTr("Elon Musk")
          font.family: "Courier New"
          font.pointSize: 10
          padding: 10
          color: "black"
          anchors.top: spaceRectMiddle.bottom
          anchors.left: buttonRectB.right
          anchors.leftMargin: 20
      }

      // Space rectangle
      Rectangle{
          id: spaceBetweenQuestions
          width: 640
          height: 50
          color: "pink"
          anchors.top: buttonText3.bottom
      }

      // Space for Button/Answers
      Loader { sourceComponent: spacers; id: spaceButAnsw2; anchors.top: spaceRectMiddle.bottom }

      // Button C
      Rectangle{
          id: buttonRectC
          width: 100
          height: 40
          color: "yellow"
          anchors.top: spaceBetweenQuestions.bottom
          anchors.left: spaceButAnsw2.right

          Text {
              id: buttonTextC
              anchors.fill: parent
              text: qsTr("C")
              font.family: "Courier New"
              font.pointSize: 14
              font.bold: true
              padding: 10
              horizontalAlignment: Text.AlignHCenter
              verticalAlignment: Text.AlignVCenter
          }
      }



      // Loader for answers
       Loader {
           id: secondAnswer
           sourceComponent: answersBox
           anchors.top: spaceBetweenQuestions.bottom
           anchors.left: buttonRectC.right
           anchors.leftMargin: 20
       }

       // Text for answer C
       Text {
           id: buttonTextD
           text: qsTr("Bjarne Stroustrup")
           font.family: "Courier New"
           font.pointSize: 10
           padding: 10
           color: "black"
           anchors.top: spaceBetweenQuestions.bottom
           anchors.left: buttonRectC.right
           anchors.leftMargin: 20
       }



       // Button D
       Rectangle{
           id: buttonRectD
           width: 100
           height: 40
           color: "yellow"
           anchors.top: spaceBetweenQuestions.bottom
           anchors.left: firstAnswerBox.right
           anchors.leftMargin: 20

           Text {
               id: buttonTextDD
               anchors.fill: parent
               text: qsTr("D")
               font.family: "Courier New"
               font.pointSize: 14
               font.bold: true
               padding: 10
               horizontalAlignment: Text.AlignHCenter
               verticalAlignment: Text.AlignVCenter
           }
       }

       // Loader for answers
        Loader {
            sourceComponent: answersBox
            anchors.top: spaceBetweenQuestions.bottom
            anchors.left: buttonRectD.right
            anchors.leftMargin: 20
        }

        // Text for last answer
        Text {
            id: buttonTextLast
            text: qsTr("Jane Doe")
            font.family: "Courier New"
            font.pointSize: 10
            padding: 10
            color: "black"
            anchors.top: spaceBetweenQuestions.bottom
            anchors.left: buttonRectD.right
            anchors.leftMargin: 20
        }

        // Space rectangle
        Rectangle{
            id: spaceBetweenLast
            width: 640
            height: 130
            color: "pink"
            anchors.top: buttonTextLast.bottom
        }

        // Space beforeQuit box
        Rectangle{
            id: spaceQuit
            width: 530
            height: 50
            color: "pink"
            anchors.top: spaceBetweenLast.bottom
        }

        // Quit box
        Rectangle{
            id: quit
            width: 110
            height: 50
            color: "pink"
            anchors.top: spaceBetweenLast.bottom
            anchors.left: spaceQuit.right
            border.color: "grey"

            Text {
                id: quitText
                text: qsTr("Quit playing")
                font.family: "Courier New"
                font.pointSize: 10
                padding: 10
                font.bold: true
                color: "black"

                anchors.leftMargin: 20
            }
        }


















}
